using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;


public class MainMenu : MonoBehaviour
{
    [SerializeField] GameObject explainMenu;
    public void OnStartGame()
    {
        Time.timeScale = 1;
        SceneManager.LoadScene(1);
    } 
    public void OnMenu()
    {
        SceneManager.LoadScene(0);
    } 
    public void OnExplain()
    {
        explainMenu.SetActive(true);
    }
    public void OnBack()
    {
        explainMenu.SetActive(false);
    }
    public void OnQuit(){
        PlayerPrefs.DeleteAll();
        Application.Quit();
    }
}
