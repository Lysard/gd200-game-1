using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UpgradeItemScript : MonoBehaviour
{
    [SerializeField] Canvas upgradeDisplay;
    private bool playerInButtonZone = false;
    [SerializeField] GameObject player;

    private CharacterScript characterScript;

    [SerializeField] string upgradeName = "DoubleJump";
    [SerializeField] Transform upgradeObjectParent;
    [SerializeField] Transform upgradeObjectDeactivatedParent;
    private float savedInventoryCount;
    private string teleportItem;
    private void Awake() {
        upgradeDisplay.enabled = false;
        

    }
    private void Start() {
        
    }
    private void Update() 
    {
        if (player != null)
        {
            
        if (player.GetComponent<RespawnScript>().savedUpgradeList.Contains(upgradeName))
        {
            Debug.Log("deactivate item");
           gameObject.transform.parent = upgradeObjectDeactivatedParent; 
        }
        }
    }


    //when the player presses the button
    public void OnButtonPress()
    {
        if (upgradeDisplay.enabled == true)
        {
            
        characterScript.upgradeList.Add(upgradeName);
        if (upgradeName == "Attack")
        {
        for (var i = 0; i < characterScript.upgradeList.Count; i++)
        {
            PlayerPrefs.SetString($"inventory item{i}", characterScript.upgradeList[i]);
            
            savedInventoryCount = i;
        }
         PlayerPrefs.SetFloat("savedInventoryCount", savedInventoryCount);
            
        }else if(upgradeName == "Teleport")
        {
           
        teleportItem = "Teleport";    
       PlayerPrefs.SetString("teleportObtained",teleportItem);
       PlayerPrefs.Save();
         
        }
        gameObject.transform.parent = upgradeObjectDeactivatedParent;
        }
    }





    // display button indicator
    private void OnTriggerEnter2D(Collider2D other) {
        if (other.tag == "Player")
        {
            characterScript = other.GetComponent<CharacterScript>();
            playerInButtonZone = true;
       Debug.Log("happened");
            upgradeDisplay.enabled = true;
        }
    }
    private void OnTriggerExit2D(Collider2D other) {
        if (other.tag == "Player")
        {
            playerInButtonZone = false;
            upgradeDisplay.enabled = false;
        }
    }



}
