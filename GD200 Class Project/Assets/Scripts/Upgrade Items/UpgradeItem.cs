using System.Collections;
using System.Collections.Generic;
using UnityEngine;


[CreateAssetMenu(fileName = "UpgradeItem", menuName = "Upgrade Items", order = 0)]
public class UpgradeItem : ScriptableObject 
{
    public UpgradeItemScript upgradeItemScript;
    public string upgradeItemName;
}

