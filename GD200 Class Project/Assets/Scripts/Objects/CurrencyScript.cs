using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CurrencyScript : MonoBehaviour
{
    [SerializeField] float currencyAmount;
    [SerializeField] Transform activeParent;
    [SerializeField] Transform currencyPool;
    
    void Start()
    {
        
    }
    private void OnTriggerEnter2D(Collider2D other) {
        if (other.tag == "Player")
        {
            
        gameObject.transform.parent.transform.parent = currencyPool;
        }
    }

    public float OnProvideAmount()
    {
        return currencyAmount;
    }
    // Update is called once per frame
    void Update()
    {
        
    }
}
