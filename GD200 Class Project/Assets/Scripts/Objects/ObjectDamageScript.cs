using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ObjectDamageScript : MonoBehaviour
{
    [SerializeField] float objectHealth;
    [SerializeField] Image healthBar;
    [SerializeField] float healthDivider;
    [SerializeField] GameObject healthDrop;
    
    
    
    
    private AttackScript playerAttackScript;
    private bool isBeingAttacked ;
    void Start()
    {
        playerAttackScript = GetComponent<AttackScript>();
        
        
       
    }


    private void Update() {
        healthBar.fillAmount = objectHealth/healthDivider;
       if (objectHealth <= 0)
       {
           
           gameObject.SetActive(false);
           Instantiate(healthDrop, gameObject.transform.position, new Quaternion(0,0,0,0));
           
       }
    }

    public void OnAttack(float objectDamage){
        
        
        Debug.Log("object takes damage");
        objectHealth -= objectDamage;
        
       
        
      
            
       
    }
    public bool AfterAttack()
    {
        return false;
    }
  



}