using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AmbienceTrigger : MonoBehaviour
{
    [SerializeField] AudioClip musicClip;


    private void OnTriggerEnter2D(Collider2D other) {

        if (other.GetComponent<CharacterScript>() != null)
        {
         
        AmbienceManager.Instance.OnChangeTrack(musicClip);
            
        }
    }
}
