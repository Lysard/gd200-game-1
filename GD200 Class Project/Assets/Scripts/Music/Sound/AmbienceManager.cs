using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AmbienceManager : MonoBehaviour
{
   public static  AmbienceManager Instance;
    
    [SerializeField] float reduceTime;
    [SerializeField] float volumeIncrease;
    [SerializeField] float volumeReduction;
    private AudioSource audioSource;
    private AudioClip musicClip;

    public enum AudioStates
    {
        Increasing,
        Decreasing,
        Stable
    }
    public AudioStates audioStates = AudioStates.Stable;
    private void Awake() {
        Instance = this;
        audioSource = GetComponent<AudioSource>();
        
    }

    private void Update() {

        if (audioSource.volume <= 0)
        {
            audioStates = AudioStates.Increasing;
        }
        switch (audioStates)
        {
            case AudioStates.Increasing:
            StartCoroutine(IncreaseVolume());
                break;
            case AudioStates.Decreasing:
            StartCoroutine(ReduceVolume());
            
                break;
            case AudioStates.Stable:
                break;
            default:
                break;
        }
        
    }
    public void OnTurnOffVolume(){
        if (audioSource.volume >0)
        {
            audioStates = AudioStates.Decreasing;
            
        }
    }

    IEnumerator ReduceVolume(){
        if (audioSource.volume >0)
        {
            Debug.Log("reducingaudio");
        audioSource.volume -= volumeReduction;
            
        }
        yield return new WaitForSeconds(reduceTime);
        if (audioSource.volume <= 0.1f)
        {
        audioSource.clip = musicClip;
        audioSource.Play();
            
        }
    }
    public void OnChangeTrack(AudioClip _musicClip){
        

        if (audioSource.clip != _musicClip)
        {
            OnTurnOffVolume();
            musicClip = _musicClip;
            
        }
        
    }

    IEnumerator IncreaseVolume(){
        if (audioSource.volume < 1)
        {
        audioSource.volume += volumeIncrease;
            
        }
        yield return new WaitForSeconds(reduceTime);
    }
}
