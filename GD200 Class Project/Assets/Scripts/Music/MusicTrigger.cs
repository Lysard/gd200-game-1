using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Audio;

public class MusicTrigger : MonoBehaviour
{
    [SerializeField] AudioClip musicClip;


    private void OnTriggerEnter2D(Collider2D other) {

        if (other.GetComponent<CharacterScript>() != null)
        {
         
        MusicManager.Instance.OnChangeTrack(musicClip);
            
        }
    }
}
