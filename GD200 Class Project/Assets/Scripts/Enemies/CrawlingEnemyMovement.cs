using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CrawlingEnemyMovement : MonoBehaviour {
    public enum EnemyStates {
        Idle,
        Walking
    }

    [SerializeField] Transform waypointParent;
    [SerializeField] EnemyStates enemyStates;
    [SerializeField] Vector3 currentTarget;
    [SerializeField] float idleTime;
    private float idleTimer;
    private int waypointChildCount;
    [SerializeField] float speed;
    [SerializeField] CrawlingEnemyRotation rotationScript;
    [SerializeField] float rotationLerp;
    //[SerializeField] CrawlerAnimation animationScript;
    private float rotation;
    
    void Start () {
        transform.position = waypointParent.GetChild (waypointChildCount).position;
        enemyStates = EnemyStates.Idle;
    }

    void Update () {
         
       

    }

    private void FixedUpdate() {
        switch (enemyStates) {
            case EnemyStates.Idle:
                
                OnIdle();
                break;
            case EnemyStates.Walking:
               
                OnWaling();
                break;
        } 
    }

    private void OnWaling()
    {
         
       transform.position = Vector3.MoveTowards(transform.position, currentTarget, speed);
       if (transform.position == currentTarget)
       {
            
           rotation += 90;
           rotationScript.OnRotationSet(rotation);
           enemyStates = EnemyStates.Idle;
       }
    }

    private void OnIdle()
    {
       
        idleTimer += 1;
       rotationLerp = idleTimer/idleTime;
        if (idleTimer >= idleTime)
        {
            
            idleTimer = 0;
            OnSetTarget(); 
        }else
        {
            rotationScript.OnRotate(rotationLerp);
        }
        
    }

    private void OnSetTarget()
    {
        waypointChildCount += 1;
        if (waypointChildCount < waypointParent.childCount)
        {
         
             
         
        currentTarget = waypointParent.GetChild(waypointChildCount).position;
        enemyStates = EnemyStates.Walking;
        }
        else
        {
            waypointChildCount = 0;
            currentTarget = waypointParent.GetChild(waypointChildCount).position;
            enemyStates = EnemyStates.Walking;

            
        }
    }
}