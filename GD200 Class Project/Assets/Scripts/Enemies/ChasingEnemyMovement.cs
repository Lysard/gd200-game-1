using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ChasingEnemyMovement : MonoBehaviour {
    public enum EnemyStates {
        Idle,
        Walking,
        Chasing,
        Surprised,
        CheckBackwards
    }

    [SerializeField] EnemyStates enemyStates;
    public bool seesPlayer;
    [SerializeField] float playerSightLenght;
    [SerializeField] LayerMask playerLayer;
    [SerializeField] Transform waypointParent;
    [SerializeField] Transform chasePoint;

    [SerializeField] Vector3 currentTarget;
    [SerializeField] float idleTime;
    [SerializeField] float surprisedTime;
    private float surprisedTimer;
    private float idleTimer;
    private int waypointChildCount;
    [SerializeField] float speed;
    [SerializeField] float runSpeed;
    private bool facingRight;
    [SerializeField] ChasingEnemyAttack chasingEnemyAttackScript;
    [SerializeField] Rigidbody2D rb2D;
    [SerializeField] float chaseStopSlowdown;
    [SerializeField] float chaseStopTimer;
    [SerializeField] ChaseEnemyAnimation animationScript;

    void Start () {
        transform.position = waypointParent.GetChild (waypointChildCount).position;
        enemyStates = EnemyStates.Idle;
    }

    void Update () {
       

    }
    private void FixedUpdate() {
       seesPlayer = Physics2D.Raycast (transform.position, transform.right * transform.localScale.x, playerSightLenght, playerLayer);
       animationScript.OnSeePlayerBool(seesPlayer);
        if (seesPlayer) {
            if (enemyStates == EnemyStates.Idle || enemyStates == EnemyStates.Walking) {

                enemyStates = EnemyStates.Surprised;
            }

        }

        OnStateUpdate ();  
    }
    private void OnStateUpdate () {
        switch (enemyStates) {
            case EnemyStates.Idle:
                OnIdle ();
                
                break;
            case EnemyStates.Walking:
            
                OnWaling ();
                break;
            case EnemyStates.Chasing:
                
                OnChasing ();
                break;
            case EnemyStates.Surprised:
                OnSurprised ();
                break;
            case EnemyStates.CheckBackwards:
                OnCheckBackwards ();
                break;
        }

    }

    private void OnSurprised () {
        surprisedTimer += 1;
        if (surprisedTimer >= surprisedTime) {
            surprisedTimer = 0;

            enemyStates = EnemyStates.Chasing;
            animationScript.OnRunningTrigger();
        }
    }

    private void OnChasing () {
         
        if (seesPlayer) {
            currentTarget = chasePoint.position;
            transform.position = Vector3.MoveTowards (transform.position, currentTarget, runSpeed);
         chaseStopTimer = 0;
        } else {
           
            transform.position = Vector3.MoveTowards (transform.position, currentTarget, runSpeed);
            
            chaseStopTimer += 1;
            if (chaseStopTimer >= chaseStopSlowdown)
            {
            enemyStates = EnemyStates.Idle;
            animationScript.OnIdleTrigger();
                chaseStopTimer = 0;
            }
        }
    }

    private void OnWaling () {
        animationScript.OnWalkingTrigger();
        transform.position = Vector3.MoveTowards (transform.position, currentTarget, speed);
        if (transform.position == currentTarget) {

            enemyStates = EnemyStates.Idle;
            animationScript.OnIdleTrigger();
        }
    }
    private void OnCheckBackwards()
    {
        transform.localScale *= -1;
        if (seesPlayer)
        {
            enemyStates = EnemyStates.Chasing;
            animationScript.OnRunningTrigger();
        }else
        {
            enemyStates = EnemyStates.Idle;
            animationScript.OnIdleTrigger();
        }
    }
   

    private void OnIdle () {
        idleTimer += 1;
        animationScript.OnIdleTrigger();
        if (idleTimer >= idleTime) {

            idleTimer = 0;
            OnSetTarget ();
        } else {

        }

    }

    private void OnSetTarget () {

        if (waypointChildCount == 0) {
            waypointChildCount = 1;

            currentTarget = waypointParent.GetChild (waypointChildCount).position;
            enemyStates = EnemyStates.Walking;
            animationScript.OnWalkingTrigger();
        } else {
            waypointChildCount = 0;
            currentTarget = waypointParent.GetChild (waypointChildCount).position;
            enemyStates = EnemyStates.Walking;
            animationScript.OnWalkingTrigger();

        }
        checkFacing ();
    }

    private void OnSeePlayer () {

    }
    private void checkFacing () {
        if (transform.position.x < currentTarget.x) {
            facingRight = true;
            Vector2 theScale = transform.localScale;
            theScale.x = 1;
            transform.localScale = theScale;
        } else if (transform.position.x > currentTarget.x) {
            facingRight = false;
            Vector2 theScale = transform.localScale;
            theScale.x = -1;
            transform.localScale = theScale;
        }
    }

}