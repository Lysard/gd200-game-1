using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CrawlingEnemyRotation : MonoBehaviour
{
    [SerializeField] GameObject enemy;
    [SerializeField] float enemyRotation;
    
    private void Start() {
        enemyRotation = 0;
        
    }
    private void Update() {
        
    }

    public void OnRotationSet(float rotation)
    {
        enemyRotation = rotation;
    }

    public void OnRotate(float t)
    {
        enemy.transform.rotation = Quaternion.Lerp(enemy.transform.rotation, Quaternion.Euler(new Vector3(0,0,-enemyRotation)),t).normalized;
        
    }

}
