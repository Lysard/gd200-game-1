using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BossBlastAttackScript : MonoBehaviour
{
    [SerializeField] Transform leftWaypoint;
    [SerializeField] Transform rightWaypoint;
    [SerializeField] Transform blastObject;
    [SerializeField] Transform bossObject;
    [SerializeField] BossMovementManager bossMovementManager;
    private int waypointNumber;
    private Transform chosenWapoint;
    private Transform targetWaypoint;
    private float waypointNumberLock;
    [SerializeField] float targetWaitTime;
    private float targetWaitTimer;
    [SerializeField] float blastSpeed;
    [SerializeField] float blastCount;
    private float blastCounter;

    private void Start() {
        blastObject.gameObject.SetActive(false);
    }

    public void OnSetBlastTarget(){
        if (waypointNumberLock == 0)
        {
         waypointNumberLock = 1;   
        waypointNumber = Random.Range(1,3);
        switch (waypointNumber)
        {
            case 1:
            chosenWapoint = leftWaypoint;
                break;
            case 2:
            chosenWapoint = rightWaypoint;
                break;
            default:
                break;
        }
        }
        bossObject.gameObject.SetActive(true);
        bossObject.position = new Vector3(chosenWapoint.position.x, chosenWapoint.position.y, 0);
        targetWaitTimer++;
        if (targetWaitTimer == targetWaitTime)
        {
            targetWaitTimer=0;
            bossMovementManager.OnSetAttackState();
        }
    } 
    private void OnTriggerEnter2D(Collider2D other) {
        blastObject.gameObject.SetActive(false);
    }

    public void OnSetBlastAttack(){
        blastObject.gameObject.SetActive(true);
         switch (waypointNumber)
        {
            case 1:
            targetWaypoint = rightWaypoint;
                break;
            case 2:
            targetWaypoint = leftWaypoint;
                break;
            default:
                break;
        }

        blastObject.position = Vector3.MoveTowards(blastObject.position, targetWaypoint.position, blastSpeed );
        if (blastObject.position == targetWaypoint.position)
        {
            waypointNumberLock = 0;
            blastCounter++;
            if (blastCounter == blastCount)
            {
                blastCounter = 0;
                 bossObject.gameObject.SetActive(false);
                 blastObject.gameObject.SetActive(false);

                 bossMovementManager.OnSetWaitingState();
                
            }else
            {
                
            bossMovementManager.OnSetTargetState();
            bossObject.gameObject.SetActive(false);
            blastObject.position = new Vector3(bossObject.position.x, bossObject.position.y, 0);
            blastObject.gameObject.SetActive(false);
            }
            
        }
    }
}
