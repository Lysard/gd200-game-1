using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BossSlamAttackScript : MonoBehaviour {
    [SerializeField] Transform slamWaypointParent;
    [SerializeField] Transform playerTarget;
    [SerializeField] float waypointParentSpeed;
    [SerializeField] float targetWaitTime;
    private float targetWaitTimer;
    [SerializeField] float attackReflexTime;
    private float attackReflexTimer;
    [SerializeField] BossMovementManager bossMovementManager;
    [SerializeField] GameObject bossEnemy;
    [SerializeField] Transform topWaypoint;
    [SerializeField] Transform groundWaypoint;
    [SerializeField] float enemySpeed;
    [SerializeField] float slamPauseTime;
    [SerializeField] float slamCounter;
    [SerializeField] float slamCount;
    private float slamPauseTimer;

    //move waypoint parent to player and then attack after a period
    public void OnSlamTarget () {

        slamWaypointParent.position = Vector3.MoveTowards (slamWaypointParent.position, new Vector3 (playerTarget.position.x, slamWaypointParent.position.y, 0), waypointParentSpeed);
        bossEnemy.transform.position = new Vector3 (topWaypoint.position.x, topWaypoint.position.y, 0);
        targetWaitTimer++;
        if (targetWaitTimer == targetWaitTime) {
            targetWaitTimer = 0;
            bossMovementManager.OnSetAttackState ();
        }

    }


    //wait and then move down to player
    public void OnSlamAttack () {
        if (attackReflexTimer != attackReflexTime) {
            attackReflexTimer++;

        }

        if (attackReflexTimer == attackReflexTime) {
            bossEnemy.transform.position = Vector3.MoveTowards (bossEnemy.transform.position, groundWaypoint.position, enemySpeed);
            Debug.Log ("enemymoves");
            if (bossEnemy.transform.position == groundWaypoint.position) {

                slamPauseTimer++;
                if (slamPauseTimer == slamPauseTime) {
                    slamPauseTimer = 0;
                    OnSlamWait ();
                }

            }
        }

    }

    //when the boss is pausing on the ground, either repeat pattern or go to next pattern
    private void OnSlamWait () {

        attackReflexTimer = 0;

        if (slamCounter == slamCount) {
            slamCounter = 0;
            if (bossMovementManager.bossPhase ==2)
            {
                
            }
            bossMovementManager.OnSetWaitingState ();
        } else {
            if (slamCounter != slamCount) {

                slamCounter++;
            }
            bossMovementManager.OnSetTargetState ();
        }
    }

}