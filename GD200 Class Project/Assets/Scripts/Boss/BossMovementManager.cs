using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BossMovementManager : MonoBehaviour {
    public enum BossStates {
        Dormant,
        Waiting,
        Target,
        Attack,
        Defeated
    }
    public BossStates bossStates;
    public int bossPhase;
    private bool playerIsInArena = false;
    [SerializeField] GameObject arenaGates;
    public int attackNumber;
    private BossSlamAttackScript bossSlamAttackScript;
    private BossSliceAttackScript bossSliceAttackScript;
    private BossBlastAttackScript bossBlastAttackScript;
    [SerializeField] EnemyHealth enemyHealth;
    [SerializeField] GameObject bossObject;
    [SerializeField] float waitTime;
    private float waitTimer;
    [SerializeField] bool isBossDefeated = false;

    private void Start () {
        bossStates = BossStates.Dormant;
        bossPhase = 0;
        bossSlamAttackScript = transform.GetComponent<BossSlamAttackScript> ();
        bossSliceAttackScript = transform.GetComponent<BossSliceAttackScript> ();
        bossBlastAttackScript = transform.GetComponent<BossBlastAttackScript> ();
        if (bossObject != null)
        {
            
        bossObject.SetActive (false);
        }
    }
    private void Update () {
        if (enemyHealth != null)
        {
            
        if (enemyHealth.enemyHealth < enemyHealth.enemyHealth/3)
        {
            OnSetBossPhase(2);
        }
        if (enemyHealth.enemyHealth <= 0)
        {
            bossStates = BossStates.Defeated;
        }
        }
        switch (bossStates) {
            case BossStates.Dormant:
                OnDormant ();
                break;
            case BossStates.Waiting:
                OnWaiting ();
                break;
            case BossStates.Target:
                OnTarget ();
                break;
            case BossStates.Attack:
                OnAttack ();
                break;
            case BossStates.Defeated:
                OnBossDefeat();
                break;

        }

    }
    private void OnBossDefeat(){
        arenaGates.SetActive(false);
    }
    public void OnBossDefeatSet(){
        isBossDefeated = true;
    }
    private void OnTriggerEnter2D (Collider2D other) {
        if (other.tag == "Player") {
            playerIsInArena = true;
        }
    }

    public void OnAttack () {
        switch (bossPhase) {
            case 0:
                break;
            case 1:
                if (attackNumber == 1) {

                    bossSlamAttackScript.OnSlamAttack ();
                } else {
                    bossSliceAttackScript.OnSliceAttack();
                }
                break;
            case 2:
            if (attackNumber == 1)
            {
                bossBlastAttackScript.OnSetBlastAttack();
            }else
            {
                bossSlamAttackScript.OnSlamAttack();
            }
                break;

            default:
                break;
        }

    }

    public void OnTarget () {
        bossObject.SetActive (true);
        switch (bossPhase) {
            case 0:
                break;
            case 1:
                if (attackNumber == 1) {
                    bossSlamAttackScript.OnSlamTarget ();
                } else {
                    bossSliceAttackScript.OnSliceTarget ();
                }
                break;
            case 2:
                if (attackNumber == 1)
                {
                   bossBlastAttackScript.OnSetBlastTarget(); 
                }else
                {
                    bossSlamAttackScript.OnSlamTarget();
                }
                break;

            default:
                break;
        }

    }

    public void OnSetAttackState () {
        bossStates = BossStates.Attack;
    }
    public void OnSetTargetState () {
        bossStates = BossStates.Target;
    }
    public void OnSetWaitingState () {
        bossStates = BossStates.Waiting;
    }
    public void OnSetBossPhase(int phase)
    {
        bossPhase = phase;
    }

    public void OnWaiting () {
        bossObject.SetActive (false);
        waitTimer++;
        if (waitTimer == waitTime) {
            waitTimer = 0;
            switch (bossPhase) {
                case 0:
                    break;
                case 1:
                    if (attackNumber == 1) {
                        attackNumber = 2;
                        bossStates = BossStates.Target;
                    }
                    break;
                case 2:
                if (!isBossDefeated)
                {
                    if (attackNumber == 2)
                    {
                        attackNumber = 1;
                        bossStates = BossStates.Target;
                    }else{
                        attackNumber = 2;
                        bossStates = BossStates.Target;
                    }
                }else
                {
                   bossStates = BossStates.Defeated; 
                }
                    
                    break;

                default:
                    break;
            }
        }

    }

    public void OnDormant () {
        if (bossPhase == 0 && playerIsInArena) {
            bossStates = BossStates.Target;
            arenaGates.SetActive (true);
            bossObject.SetActive (true);
           OnSetBossPhase(1);
            attackNumber = 1;
        }

    }
}