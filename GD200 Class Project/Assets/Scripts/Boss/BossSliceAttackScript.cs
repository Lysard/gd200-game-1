using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BossSliceAttackScript : MonoBehaviour
{
    [SerializeField] Transform waypointParent;
    [SerializeField] GameObject player;
    [SerializeField] float parentSpeed;
    [SerializeField] GameObject bossObject;
    [SerializeField] Transform frontWaypoint;
    [SerializeField] Transform backWaypoint;
    [SerializeField] float bossSpeed;
    [SerializeField] float sliceSpeed;
    [SerializeField] float targetWaitTime;
    [SerializeField] BossMovementManager bossMovementManager;
    private float targetWaitTimer;
    [SerializeField] float sliceCount;
    [SerializeField] float sliceCounter;
    private void Start() {
      
    }
    public void OnSliceTarget(){
        waypointParent.position = Vector3.MoveTowards(waypointParent.position, new Vector3(player.transform.position.x, waypointParent.position.y, 0), parentSpeed);
        waypointParent.localScale = player.transform.localScale;
        bossObject.SetActive(false);
        bossObject.transform.position = Vector3.MoveTowards(bossObject.transform.position, frontWaypoint.position, bossSpeed );
        targetWaitTimer++;

        if (targetWaitTimer == targetWaitTime)
        {
            targetWaitTimer = 0;
           bossMovementManager.OnSetAttackState(); 
        }
    }
    public void OnSliceAttack(){

       bossObject.SetActive(true);
        bossObject.transform.position = Vector3.MoveTowards(bossObject.transform.position, backWaypoint.position, sliceSpeed );
        if (bossObject.transform.position == backWaypoint.position)
        {
            sliceCounter++;
            if (sliceCounter == sliceCount)
            {
                bossObject.SetActive(false);
                sliceCounter = 0;
                bossMovementManager.OnSetBossPhase(2);
                bossMovementManager.OnSetWaitingState();
            }else
            {
            bossMovementManager.OnSetTargetState();
                
            }
        }
    }


}
