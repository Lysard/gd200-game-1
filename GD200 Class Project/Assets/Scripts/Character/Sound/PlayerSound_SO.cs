using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using UnityEngine.Audio;

[CreateAssetMenu(fileName = "PlayerSound_SO", menuName = "SoundContainerObject/PlayerSound_SO", order = 0)]
public class PlayerSound_SO : ScriptableObject {
    public AudioClip swordSlash1;
    public AudioClip swordSlash2;
    public AudioClip jump;
}
