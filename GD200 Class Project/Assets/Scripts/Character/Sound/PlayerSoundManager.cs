using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Audio;

public class PlayerSoundManager : MonoBehaviour
{
    public static PlayerSoundManager Instance;

    private void Awake() {
        Instance = this;
    }

    public void PlaySound(AudioClip soundEffect){
        GetComponent<AudioSource>().clip = soundEffect;
        GetComponent<AudioSource>().Play();
    }
}
