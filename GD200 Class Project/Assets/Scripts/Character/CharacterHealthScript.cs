using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using TMPro;


public class CharacterHealthScript : MonoBehaviour
{
    public float playerHealth;
    public float maxHealth;
    [SerializeField] float contactDamage;
    [SerializeField] Image healthBar;
    public bool hasDied;
    [SerializeField] float healthPickup;

    [SerializeField] RespawnScript respawnScript;
    private Collider2D healthCollider;
    [SerializeField] Rigidbody2D rb2D;
    [SerializeField] Vector2 forcePoint;
    [SerializeField] float knockbackMultiplier;

    [SerializeField] float knockbackTimer;
    [SerializeField] float knockbackMovementTimer;
    [SerializeField] CharacterKnockback characterKnockbackScript;
    public float healingAmount;
    [SerializeField] TextMeshProUGUI healingText;
    
    private Vector2 knockBackForce;
    public bool isKnocked = false;
    private void Start() {
      healingAmount = PlayerPrefs.GetFloat("playerHealingAmount"); 
    }
    public void OnSaveHeal()
    {
        playerHealth = maxHealth;
    }
    public void OnHealBuy()
    {
        healingAmount = 3;
    }
    public void OnHeal()
    {
        if (healingAmount > 0 && playerHealth != maxHealth)
        {
            playerHealth = maxHealth;
            healingAmount --;
        }
    }

    private void Update() {
        healthBar.fillAmount = playerHealth/100;
        healingText.text = healingAmount.ToString();
        if (playerHealth <= 0)
        {
            Debug.Log("died");
           if (PlayerPrefs.GetFloat("saveCount") <= 1)
           {
               
           respawnScript.OnSwordRespawn();
           }else
           {
               respawnScript.OnRespawn();
           }
           
           
        }
        if (playerHealth > maxHealth)
        {
            playerHealth = maxHealth;
        }
    }

    private void OnTriggerEnter2D(Collider2D other) {

        
        if (other.tag == "EnemyDamage" )
        {
            forcePoint = transform.position - other.gameObject.transform.position;
            playerHealth -= contactDamage;
           
            characterKnockbackScript.OnKnockback(forcePoint);
          
            
            
        }
        if (other.tag == "Spike")
        {
             playerHealth -= contactDamage;
           respawnScript.OnSpikeRespawn();
        }

        if (other.tag == "HealthDrop")
        {
            Debug.Log("picked up health");
           playerHealth += healthPickup; 
           other.gameObject.SetActive(false);
        }
    }

   
}
