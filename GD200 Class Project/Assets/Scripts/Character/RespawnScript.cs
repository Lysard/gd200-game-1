using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class RespawnScript : MonoBehaviour {
    private Vector3 respawnPoint;
    public Vector3 deathRespawnPoint;
    [SerializeField] CharacterHealthScript characterHealthScript;
    [SerializeField] float respawnSpeed;
    private GameObject character;
    private Vector2 playerSavedPosition;
    [SerializeField] Rigidbody2D rB2d;
    [SerializeField] CharacterScript characterScript;
    private float savedInventoryCount;
    private float saveCount;
    private string teleportItem;

    public List<string> savedUpgradeList = new List<string> ();

    // Update is called once per frame
    private void Awake () {
        if (PlayerPrefs.GetString("teleportObtained") != "")
        {
            characterScript.OnRespawnTeleportPoint(PlayerPrefs.GetString("teleportObtained"));
        }
        if (PlayerPrefs.GetFloat ("saveCount") > 0) {
            rB2d.position = new Vector2 (PlayerPrefs.GetFloat ("XPosition"), PlayerPrefs.GetFloat ("YPosition"));
            for (var i = -1; i < PlayerPrefs.GetFloat ("savedInventoryCount"); i++) {
                savedUpgradeList.Add (PlayerPrefs.GetString ($"inventory item{i+1}"));
                characterScript.OnRespawnInventory (savedUpgradeList[i + 1]);
            }
        }

    }
    public void OnRespawn () {
        
        SceneManager.LoadScene (1);

    }
    public void OnSpikeRespawn () {
        rB2d.position = respawnPoint;
    }

    private void OnTriggerEnter2D (Collider2D other) {

        switch (other.tag) {
            case "RespawnPoint":
                respawnPoint = other.gameObject.transform.position;
                break;
            case "SavePointSword":
                Debug.Log (" respawnpoint triggered");
                deathRespawnPoint = other.gameObject.transform.position;
                PlayerPrefs.SetFloat ("XPosition", deathRespawnPoint.x);
                PlayerPrefs.SetFloat ("YPosition", deathRespawnPoint.y);

                break;
        }

    }
    public void OnSwordRespawn () {
        
        saveCount = 1;
        
        PlayerPrefs.SetFloat ("saveCount", saveCount);
        PlayerPrefs.Save ();
        SceneManager.LoadScene (1);
    }
}