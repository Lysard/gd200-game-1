using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class CharacterBalanceScript : MonoBehaviour
{
    public float currencyBalance;
    [SerializeField] TextMeshProUGUI balanceText;

    void Start()
    {
         currencyBalance = PlayerPrefs.GetFloat("playerBalance");
    }
    private void Update() {
        balanceText.text = currencyBalance.ToString();
    }
    public void OnBuyItem(float buyAmount)
    {
        currencyBalance -= buyAmount;
    }
    public void OnCurrencyPickup(float pickupAmount)
    {
        currencyBalance += pickupAmount;
    }

    private void OnTriggerEnter2D(Collider2D other) {
        if (other.tag == "Currency")
        {
            if (other.GetComponent<CurrencyScript>() != null)
            {
                
            OnCurrencyPickup(other.GetComponent<CurrencyScript>().OnProvideAmount());
            }
        }
    }
    
}
