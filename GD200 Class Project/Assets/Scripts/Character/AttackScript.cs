using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

public class AttackScript : MonoBehaviour {
    // Start is called before the first frame update

    
    private ObjectDamageScript objectDamageScript;
    private EnemyHealth enemyHealthScript;

    private CharacterControls inputs;
    public bool isSwinging;
    public bool isHitting;
    public float objectDamage = 20;
    [SerializeField] float chargeMeter;
    [SerializeField] float chargeCount;
    public bool isDoneChargeing;
    [SerializeField] float chargeTime;
    private bool dashAttackUsed;
    private bool dashAttackReady;
    private bool isDashing;
    private Collider2D healthCollider;
    private SpriteRenderer weaponSprite;
    [SerializeField] float dashTimer;

    [SerializeField] SpriteRenderer characterSprite;
    [SerializeField] CharacterScript characterMovementScript;
    [SerializeField] AnimationManager animationManager;

    [SerializeField] float enemyDamage;
    private Vector2 moveInput;
    [SerializeField] Transform weapon;
    private Vector2 weaponXPosition;
    public enum PlayerLookState {
        LookingDown,
        LookingUp,
        NotLooking
    }
    private PlayerLookState playerLookState;
    [SerializeField] Rigidbody2D rb2D;
    [SerializeField] float slashJumpMultiplier;
    [SerializeField] float swingTime;
    private bool isAttacking;
    [SerializeField] PlayerSound_SO playerSoundContainer;

    private void Awake () {

    }
    void Start () {
        inputs = new CharacterControls ();
        inputs.Enable ();
        inputs.CharacterMovement.Attack.performed += OnSwing;
        //inputs.CharacterMovement.Charge.performed += OnCharge;
        inputs.CharacterMovement.Dash.performed += OnDash;
        inputs.CharacterMovement.LookUpDown.performed += OnUpDownLook;
        weaponSprite = transform.GetChild (0).GetComponent<SpriteRenderer> ();
        healthCollider = transform.GetChild (1).GetComponent<Collider2D> ();
        weaponXPosition = weapon.transform.position;

    }

    private void OnUpDownLook (InputAction.CallbackContext obj) {
        Debug.Log ("look performed");
        moveInput = inputs.CharacterMovement.LookUpDown.ReadValue<Vector2> ();
        switch (moveInput.y) {
            case <0:
                playerLookState = PlayerLookState.LookingDown;
                break;
            case >0:
                playerLookState = PlayerLookState.LookingUp;
                break;
            case 0:
                playerLookState = PlayerLookState.NotLooking;
                break;
        }

    }

    private void OnDestroy () {
        inputs.CharacterMovement.Attack.performed -= OnSwing;
        inputs.CharacterMovement.Dash.performed -= OnDash;
        inputs.CharacterMovement.LookUpDown.performed -= OnUpDownLook;
    }

    private void Update () {
        
     


    }
    private void FixedUpdate() {
       if (isDoneChargeing) {
            Debug.Log ("charge fired up to be used");
        }

        WhenSwinging ();
        WhenDashing ();
        
    }

    // FUNCTIONS

    private void WhenSwinging () {

        if (isSwinging && isHitting) {
            Debug.Log ("attacked");
            if (characterMovementScript.isGrounded == false && playerLookState == PlayerLookState.LookingDown)
            {
                
                rb2D.velocity = Vector2.zero;
                rb2D.AddForce(rb2D.transform.up * slashJumpMultiplier,ForceMode2D.Impulse);
            }
            //objectDamageScript.OnAttack(objectDamage);
            enemyHealthScript.OnAttack (enemyDamage);
            isSwinging = enemyHealthScript.AfterAttack ();

        } else {
            Debug.Log ("notattacking");

        }
    }
   
    private void WhenDashing () {

        // if ( !dashAttackUsed && isHitting && isDashing)
        // {

        //    objectDamageScript.OnAttack(objectDamage*2);
        //        dashAttackReady = objectDamageScript.AfterAttack(); 
        //        characterSprite.color = new Color(1,0.4198113f,0.4198113f,1);
        //        Debug.Log("dash attack used");
        //        dashAttackUsed = true;
        //        isDoneChargeing = false;
        // }
        // if (!dashAttackUsed && isDashing)
        // {
        //  // turn off health collider
        //     healthCollider.enabled = false;
        //     if (healthCollider.enabled == false)
        //     {
        //         Debug.Log("turned off health collider");
        //     }
        //     StartCoroutine(OnHealthColliderDash());
        // characterSprite.color = new Color(1,0.4198113f,0.4198113f,1);

        // }
    }

    // TRIGGERS
    void OnTriggerEnter2D (Collider2D other) {
        if (other.tag == "EnemyHealth") {
            isHitting = true;

            //objectDamageScript = other.GetComponent<ObjectDamageScript>();
            enemyHealthScript = other.GetComponent<EnemyHealth> ();
            //Debug.Log("ontrigger");

        }
    }
    private void OnTriggerExit2D (Collider2D other) {
        isHitting = false;
    }

    // BUTTONS PRESSED

    // private void OnCharge(InputAction.CallbackContext obj)
    // {

    //     if (chargeCount != chargeMeter)
    //     {
    //         isDoneChargeing = false;
    //         Debug.Log("is charging");
    //         StartCoroutine(OnCharging());
    //     }

    // }
    private void OnSwing (InputAction.CallbackContext obj) {
      PlayerSoundManager.Instance.PlaySound(playerSoundContainer.swordSlash1);
     if (characterMovementScript.isGrounded == false && playerLookState == PlayerLookState.LookingDown && characterMovementScript.upgradeList.Contains("Attack"))
     {
         animationManager.OnDownSwingTrigger();
     
     }else if(characterMovementScript.upgradeList.Contains("Attack"))
     {
         animationManager.OnSwingTrigger(); 
     }
     StartCoroutine(OnSwingWait());
    }
    IEnumerator OnSwingWait()
    {
        yield return new WaitForSeconds(swingTime   );
         if (characterMovementScript.upgradeList.Contains ("Attack")) {
            isSwinging = true;
            if (characterMovementScript.isGrounded == false && playerLookState == PlayerLookState.LookingDown) {
                if (weapon.rotation == Quaternion.Euler (new Vector3 (0, 0, 0))) {

                    weapon.rotation = Quaternion.Euler (new Vector3 (0, 0, -90 * Mathf.RoundToInt (transform.localScale.x)));
                }

            } else {
                weapon.rotation = Quaternion.Euler (new Vector3 (0, 0, 0));

            }
           
            StartCoroutine (OnSwinging ());

        }

    }
    private void OnDash (InputAction.CallbackContext obj) {
        isDashing = true;

        StartCoroutine (OnDashing ());

    }
    IEnumerator OnSwinging () {
        yield return new WaitForSeconds (0.1f);
        isSwinging = false;
       

    }
    // IEnumerator OnHealthColliderDash()
    // {
    //     yield return new WaitForSeconds(1);
    //     healthCollider.enabled = true;
    //     if (healthCollider.enabled == true)
    //     {
    //       Debug.Log("healthcollider on again");   
    //     }
    // }
    IEnumerator OnDashing () {
        yield return new WaitForSeconds (dashTimer);

        isDashing = false;

    }
    // IEnumerator OnCharging(){
    //     Debug.Log("waiting");
    //     yield return new WaitForSeconds(chargeTime);

    //     chargeCount ++;

    //     if (chargeCount == chargeMeter)
    //     {
    //         isDoneChargeing = true;
    //         characterSprite.color = new Color(0,1,0,1);
    //         Debug.Log("is done charging");
    //         chargeCount = 0;
    //         dashAttackUsed = false;
    //     }

    // }

}