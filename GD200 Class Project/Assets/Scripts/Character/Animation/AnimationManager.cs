using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Audio;
using UnityEngine.ParticleSystemJobs;

public class AnimationManager : MonoBehaviour
{
    Animator animator;
    private float attackNumber;
    [SerializeField] float attackChangeTime;
    [SerializeField] PlayerSound_SO playerSoundContainer;
    [SerializeField] ParticleSystem _particleSystem;
    private bool isGrounded;
    private float _xMovement;
    float flipSide;
    
    void Start()
    {
        animator = GetComponent<Animator>();
    }

    // Update is called once per frame
    void Update()
    {
        animator.SetFloat("AttackNumber",attackNumber);
    }
    public void OnGetMovement(float xMovement) {
        animator.SetFloat("XMovement", xMovement);
       
    }
    public void OnGetVelocityY(float yMovement){
        if (yMovement != 0 )
        {
            switch (yMovement )
            {
                case >0:
                animator.SetFloat("YMovement", yMovement/yMovement);
                    break;
                case <0:
                animator.SetFloat("YMovement", -yMovement/yMovement);
                    break;
                default:
                    break;
            }
            
            
        
        }else
        {
            animator.SetFloat("YMovement", yMovement);
        }

        

    }
    public void OnPlayRunSound()
    {
        
        GetComponent<AudioSource>().Play();
    }

    public void OnSwingTrigger(){
        
        animator.SetTrigger("AttackTrigger");
        
        
        
        StartCoroutine(OnAttackNumberChange());
    }
    public void OnDownSwingTrigger(){
        animator.SetTrigger("DownAttackTrigger");
    }
    public void OnDashTrigger(){
        animator.SetTrigger("DashTrigger");
    }

    IEnumerator OnAttackNumberChange(){
        yield return new WaitForSeconds(attackChangeTime);
       
        if (attackNumber == 0)
        {
            attackNumber = 1;
        }
        else
        {
            attackNumber = 0;
        }
    }

    public void OnGetDoubleJumpCount(float count){
        animator.SetFloat("doubleJumpCount", count);
    }
    public void OnGetWallSlideBool(bool isWallSliding){
        animator.SetBool("isWallsliding", isWallSliding);
    }
    public void OnGetGroundedBool(bool _isGrounded){
        animator.SetBool("isGrounded", _isGrounded);
        isGrounded = _isGrounded;
    }

    public void OnParticlePlay(){
        
       if (transform.parent.transform.localScale.x < 0)
       {
           flipSide = -1;
       }else if (transform.parent.transform.localScale.x > 0)
       {
           flipSide = 1;
       }
        _particleSystem.Play();
        _particleSystem.transform.localScale = new Vector3( -flipSide, 1 , 1);

    }
    public void OnParticleStop(){
        _particleSystem.Stop(); 
    }

}
