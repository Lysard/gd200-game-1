using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CharacterKnockback : MonoBehaviour
{
    [SerializeField] Vector2 forcePoint;
    [SerializeField] Rigidbody2D rb2D;
    [SerializeField] float knockbackMultiplier;

    
    
    private void Update() {
       
    }
    
    public void OnKnockback(Vector2 forcePoint)
    {
         
           rb2D.velocity = Vector2.zero;
           rb2D.MovePosition(new Vector2(rb2D.position.x + forcePoint.x, rb2D.position.y + forcePoint.y));
           
    }
     
    
}
