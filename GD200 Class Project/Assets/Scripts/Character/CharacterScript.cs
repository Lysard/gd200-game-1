using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;
using UnityEngine.SceneManagement;
using UnityEngine.Tilemaps;

public class CharacterScript : MonoBehaviour {
    public List<string> upgradeList = new List<string> ();
    [SerializeField] Rigidbody2D rB2d;
    private CharacterControls inputs;
    private Vector2 moveInput;
    private AttackScript attackScript;

    private float doubleJumpCount;
    private float moveX;
    private float dashCooldown;

    public bool isGrounded;
    private bool isInAir;
    private bool isDashing;
    private bool isGoingRight;
    private bool isOnWallRight;
    private bool isOnWallLeft;
    private bool isWallSliding;
    private bool isStandingOnWall;
    private bool isMovingAwayFromWallLeft;
    private bool isMovingAwayFromWallRight;
    private Vector2 facingDirection;
    Vector3 transformScale;
    [SerializeField] float wallRaycastLenght;
    [SerializeField] float groundRaycastLength;

    [SerializeField] float wallJumpY;
    [SerializeField] float wallSlidingSpeed;
    [SerializeField] float wallSlideMaxValue;
    [SerializeField] float dashTimer = 0;
    [SerializeField] Vector2 wallForce;
    [SerializeField] Vector2 dashMultiplier;
    [SerializeField] float doubleJump;
    [SerializeField] float speedMultiplier;
    [SerializeField] float jumpMultiplier;
    [SerializeField] LayerMask groundLayer;
    [SerializeField] LayerMask wallLayer;

    private UpgradeItemScript upgradeItemScript;
    private bool playerInButtonZone = false;

    [SerializeField] GameObject switchTileMap;
    [SerializeField] CharacterHealthScript characterHealthScript;
    [SerializeField] GameObject player;
    [SerializeField] ManualSaveScript manualSaveScript;
    [SerializeField] ShopUI shopUIScript;
    [SerializeField] AnimationManager animationManager;
    [SerializeField] PlayerSound_SO playerSoundContainer;

    private void Awake () {
        upgradeList.Add ("DoubleJump");
        upgradeList.Add ("Attack");
        upgradeList.Add ("Dash");
        upgradeList.Add ("WallJump");

    }
    public void OnRespawnTeleportPoint (string teleportPoint) {
        upgradeList.Add (teleportPoint);
    }
    public void OnRespawnInventory (string savedItem) {
        upgradeList.Add (savedItem);
    }
    public void PlayerInButtonZone (bool inInZone) {
        playerInButtonZone = inInZone;
    }
    void Start () {

        //rB2d.position = new Vector2(PlayerPrefs.GetFloat("xPosition"), PlayerPrefs.GetFloat("yPosition"));
        inputs = new CharacterControls ();
        inputs.Enable ();
        inputs.CharacterMovement.Move.performed += OnMove;
        inputs.CharacterMovement.Jump.performed += OnJump;
        inputs.CharacterMovement.Dash.performed += OnDash;
        inputs.CharacterMovement.Interact.performed += OnInteract;
        inputs.CharacterMovement.Heal.performed += OnHeal;
        dashCooldown = dashTimer;
        doubleJumpCount = 0;
        transformScale = transform.localScale;

    }
    public void OnTeleport (Vector3 positon) {
        rB2d.position = new Vector3 (positon.x, positon.y, 0);
    }

    private void OnHeal (InputAction.CallbackContext obj) {
        if (characterHealthScript != null) {

            characterHealthScript.OnHeal ();
        }
    }

    private void OnInteract (InputAction.CallbackContext obj) {
        if (playerInButtonZone) {
            manualSaveScript.OnSave ();
            if (upgradeItemScript != null) {

                upgradeItemScript.OnButtonPress ();
            }
            shopUIScript.OnShopInteraction ();

        }

    }

    private void OnDestroy () {
        inputs.CharacterMovement.Move.performed -= OnMove;
        inputs.CharacterMovement.Move.performed -= OnDash;
        inputs.CharacterMovement.Move.performed -= OnJump;
        inputs.CharacterMovement.Interact.performed -= OnInteract;
        inputs.CharacterMovement.Heal.performed -= OnHeal;

    }

    private bool stopMovement = false;
    public void StopMovement (bool stop) {
        rB2d.velocity = Vector2.zero;
        stopMovement = stop;
    }

    private void FixedUpdate () {
        if (!stopMovement) {

            Debug.Log (rB2d.velocity.y);
            OnMoveInput ();
            OnJumpInput ();
            OnDashInput ();
            OnWall ();

        }

    }
    private void Update () {
        GetVariablesForAnimator ();
    }

    public void GetVariablesForAnimator () {
        animationManager.OnGetVelocityY (rB2d.velocity.y);
        animationManager.OnGetGroundedBool (isGrounded);
        animationManager.OnGetDoubleJumpCount (doubleJumpCount);
        animationManager.OnGetWallSlideBool (isWallSliding);

    }

    private void OnMoveInput () {
        rB2d.velocity = new Vector2 (moveInput.x * speedMultiplier * Time.deltaTime, rB2d.velocity.y);

        // this is so im not stuck on the wall when i am on the ground
        if ((isOnWallRight && isGrounded) || (isOnWallLeft && isGrounded)) {
            isOnWallRight = false;
            isOnWallLeft = false;

            //Debug.Log("is not on wall");
        }
        // rotate object in direction of movement

        switch (moveX) {
            case <0:
                Vector3 tempVar = transformScale;
                tempVar.x *= -1;
                transform.localScale = tempVar;
                Debug.Log ("moving left");
                break;
            case >0:
                Vector3 tempVar2 = transformScale;
                tempVar = new Vector3 (1, 1, 1);
                transform.localScale = tempVar2;
                Debug.Log ("moving right");
                break;
            default:
                break;
        }

    }
    private void OnJumpInput () { // ground check
        isGrounded = Physics2D.Raycast (transform.position, -transform.up, groundRaycastLength, groundLayer);
        isStandingOnWall = Physics2D.Raycast (transform.position, -transform.up, wallRaycastLenght, wallLayer);
        // if i am on ground or on wall the double jump is reset and i am not in the air anymore
        if ((isGrounded || isOnWallLeft || isOnWallRight || isStandingOnWall)) {
            isInAir = false;
            doubleJumpCount = 0;
            //Debug.Log("is on ground");

        }
        if ((!isGrounded && !isOnWallRight && !isOnWallLeft && !isStandingOnWall)) {
            isInAir = true;

            //Debug.Log("is in air");

        }
    }
    private void OnDashInput () {
        // the dash time counts down so that you cannot spam dash on after the other
        if (isDashing) {
            dashCooldown--;
        }
        // only if dashcooldown/max dash amount is under zero can you dash again. dash cooldown is reset to the dash timer amount
        // dashing is also false
        if (dashCooldown < 0) {
            dashCooldown = dashTimer;
            isDashing = false;
        }
    }

    private void OnWall () {
        // is character on wall?
        isOnWallRight = Physics2D.Raycast (transform.position, transform.right, wallRaycastLenght, wallLayer);
        isOnWallLeft = Physics2D.Raycast (transform.position, -transform.right, wallRaycastLenght, wallLayer);
        facingDirection = transform.right;

        //When on wall i am sliding, if not no sliding. The wall sliding check!!
        switch ((isOnWallRight) || (isOnWallLeft)) {
            case true:
                WallSliding ();

                break;
            case false:
                isWallSliding = false;
                //Debug.Log("is not sliding");
                break;

        }

        // checks if I am moving in opposite direction of the wall also says i am no longer wall sliding
        if (moveX <= 0 && isOnWallRight) {

            isWallSliding = false;

        }
        if (moveX >= 0 && isOnWallLeft) {

            isWallSliding = false;

        }

        //what happens when i am sliding
        if (isWallSliding) {
            Debug.Log ("successfully sliding");
            // if the wallsliding check is true, i start sliding down
            rB2d.velocity = new Vector2 (rB2d.velocity.x, Mathf.Clamp (rB2d.velocity.y, -wallSlidingSpeed, wallSlideMaxValue));

        } else {
            Debug.Log ("successfully stopped sliding");
            // if the wallsliding check is false i stop sliding
            rB2d.velocity = new Vector2 (moveInput.x * speedMultiplier * Time.deltaTime, rB2d.velocity.y);
        }
    }
    IEnumerator OnWallSlideEndTime () {
        yield return new WaitForSeconds (0.1f);
        isWallSliding = false;
    }

    private void WallSliding () {
        //if not grounded
        if (!isGrounded && upgradeList.Contains ("WallJump")) {
            //if i am moving in direction of the walls and i am on the wall I am wall sliding
            if (moveX >= 0 && isOnWallRight) {

                isWallSliding = true;
                Debug.Log ("is wall sliding");
            }
            if (moveX <= 0 && isOnWallLeft) {

                isWallSliding = true;
                Debug.Log ("is wall sliding");

            }

        }

    }

    // BUTTONS BEING PRESSED
    private void OnMove (InputAction.CallbackContext incomingValue) {
        if (characterHealthScript.isKnocked == false) {

            //moving 
            moveInput = inputs.CharacterMovement.Move.ReadValue<Vector2> ();
            // I am getting the x vector of the move input to use it as a value later
            moveX = moveInput.x;
            animationManager.OnGetMovement (moveX);
        }

    }
    private void OnJump (InputAction.CallbackContext incomingValue) {
        if (rB2d != null) {
            PlayerSoundManager.Instance.PlaySound (playerSoundContainer.jump);
            // jump
            if (!isInAir && !isOnWallLeft && !isOnWallRight) {

                rB2d.velocity = new Vector2 (rB2d.velocity.x, jumpMultiplier * Time.fixedDeltaTime);
                isInAir = true;
                //Debug.Log("is jumping");

            }

            // if i have the double jump item I can double jump
            if (upgradeList.Contains ("DoubleJump")) {

                // double jump (only 1 double jump allowed)
                if (!isGrounded && doubleJumpCount < 1 && !isOnWallLeft && !isOnWallRight) {

                    rB2d.velocity = new Vector2 (rB2d.velocity.x, doubleJump * Time.fixedDeltaTime);
                    doubleJumpCount++;
                    Debug.Log ("is double jumping");
                }
            }

            if (upgradeList.Contains ("WallJump")) {

                // jump up while on wall
                if ((isOnWallRight || isOnWallLeft)) {

                    if (isWallSliding) {
                        //Debug.Log("is wall jumping");
                        rB2d.velocity = Vector2.zero;

                        //rB2d.velocity = new Vector2(rB2d.velocity.x,wallJumpY);
                        // X PUSH OFF WALL WHEN JUMPING

                        rB2d.AddForce (new Vector2 (wallForce.x * -transform.localScale.x, wallForce.y), ForceMode2D.Force);

                    }

                }
            }

        }

    }

    private void OnDash (InputAction.CallbackContext obj) {
        if (rB2d != null) {

            if (upgradeList.Contains ("Dash")) {

                isDashing = true;

                //dash only available when dash timer is replenished
                if (dashCooldown == dashTimer) {
                    // dash in direction of x movement!

                    //put dash trigger here
                    switch (moveX) {
                        case >0:
                            PlayerSoundManager.Instance.PlaySound (playerSoundContainer.swordSlash2);
                            animationManager.OnDashTrigger ();
                            
                            StartCoroutine(OnDashRightWait());
                            break;
                        case <0:
                            PlayerSoundManager.Instance.PlaySound (playerSoundContainer.swordSlash2);
                            animationManager.OnDashTrigger ();
                           StartCoroutine(OnDashLeftWait());
                            break;
                        default:
                            break;
                    }

                }
            }
        }

    }
    IEnumerator OnDashRightWait(){
        yield return new WaitForSeconds(0.1f);
         rB2d.AddForce (transformScale * dashMultiplier, ForceMode2D.Force);
    }
    IEnumerator OnDashLeftWait(){
        yield return new WaitForSeconds(0.1f);
         rB2d.AddForce (-transformScale * dashMultiplier, ForceMode2D.Force);
    }

    // get upgrade item script
    private void OnTriggerEnter2D (Collider2D other) {
        if (other.tag == "UpgradeItem") {
            upgradeItemScript = other.GetComponent<UpgradeItemScript> ();

            playerInButtonZone = true;
        }
        switch (other.tag) {
            case "UpgradeItem":
                upgradeItemScript = other.GetComponent<UpgradeItemScript> ();

                playerInButtonZone = true;
                break;
            case "SavePoint":
                manualSaveScript = other.GetComponent<ManualSaveScript> ();
                break;
            case "Shop":
                shopUIScript = other.GetComponent<ShopUI> ();
                break;

            default:
                break;
        }

    }
    private void OnTriggerExit2D (Collider2D other) {
        if (other.tag == "UpgradeItem") {
            upgradeItemScript = other.GetComponent<UpgradeItemScript> ();
            playerInButtonZone = false;
        }
    }

}