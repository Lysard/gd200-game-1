using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DialogueCharacterManager : MonoBehaviour
{
    public enum DialogueStates
    {
        Quiet,
        Talking
    }
    public DialogueStates dialogueStates;
    [SerializeField] List<Sprite> spriteList = new List<Sprite>();
    [SerializeField] Image dialogueCharacter;
    void Start()
    {
        dialogueStates = DialogueStates.Quiet;
        dialogueCharacter.sprite = spriteList[0];
    }

    public void OnTalking(){
        dialogueCharacter.sprite = spriteList[1];
        dialogueStates = DialogueStates.Talking;
    }
    public void OnQuiet(){
        dialogueCharacter.sprite = spriteList[0];
        dialogueStates = DialogueStates.Quiet;
    }
}
