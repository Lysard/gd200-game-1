using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

public class DialogueInput : MonoBehaviour
{
    private CharacterControls inputs; 
    public bool canType = false;
    [SerializeField] DialogueMovement dialogueMovement;
    private bool dialogueStarted = false;
    [SerializeField] Canvas shopCanvas;
    void Start()
    {
     inputs = new CharacterControls();
     inputs.CharacterMovement.Enable();
    inputs.CharacterMovement.Interact.performed += OnInteract;
    }

    private void OnInteract(InputAction.CallbackContext obj)
    {
       if (!canType && dialogueStarted && shopCanvas.enabled == true )
        {
            
        Debug.Log("working");
        dialogueMovement.OnDialogueClick();
        
        }
    }
    public void dialogueEnded(){
        dialogueStarted = false;
    }

    public void CancleType()
    {
        canType = false;
    }
    public void StartType()
    {
        canType = true;
    }

    public void OnClick()
    {
        if (shopCanvas.enabled == true)
        {
        dialogueStarted = true;
        dialogueMovement.OnAppear();
            
        }
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
