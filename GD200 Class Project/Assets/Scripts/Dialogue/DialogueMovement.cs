using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class DialogueMovement : MonoBehaviour {
    [SerializeField] TextMeshProUGUI text;
    private string textBubble1;
    private int countAmount;
    [SerializeField] int counter;
    private string dialogueOut;
    [SerializeField] float time;
    [SerializeField] float minTime;
    [SerializeField] float lockTime;
    [SerializeField] DialogueOutput dialogueOutput;
    [SerializeField] DialogueInput dialogueInput;
    private int dialogueIndex = 0;
    [SerializeField] float interactionAmount;
    [SerializeField] float maxInteractionAmount;
    [SerializeField] GameObject textBubble;
    [SerializeField] GameObject dialogueCharacter;
    [SerializeField] DialogueCharacterManager dialogueCharacterManager;
    private bool dialogueDone = false;
    [SerializeField] float maxTime;

    [SerializeField] int dialogueListAmountCounter;
    private void Start () {
        minTime = time;
        OnDisappear ();
        counter = 0;
    }

    private void FixedUpdate () {
        if (dialogueInput.canType) {

            if (lockTime == 0) {
                Debug.Log("typing");
                StartCoroutine (OnTypeUpdate ());
                lockTime = 1;
            }
        }

    }
    public void OnDialogueClick () {

        OnAppear ();

        OnWipeText ();
        OnTyping ();
    }
    public void OnDialogueHold(){
     
      time = maxTime;  
         
    }
    public void OnDialogueUnHold(){
        time = minTime;
    }
    
    private void OnTyping () {
        if (dialogueInput.canType == false && dialogueIndex < dialogueOutput.feedDialogueList.Count) {
            textBubble1 += dialogueOutput.OnInsertDialogue (dialogueIndex);
            Debug.Log("start typing");
            countAmount = textBubble1.Length - 1;

            dialogueIndex++;

            dialogueInput.StartType ();
            interactionAmount--;
        }

    }
    private void OnWipeText () {
        if (interactionAmount <= 0) {
            textBubble1 = "";
            text.text = "";
            counter = 0;
            interactionAmount = maxInteractionAmount;
            

        }

    }
    private void OnDisappear () {
        Debug.Log ("Ondisappear");
        if (text.text == "") {
            Debug.Log ("dissapeared");
            textBubble.SetActive (false);
            dialogueCharacter.SetActive (false);
            dialogueInput.dialogueEnded();

        }
    }

    public void OnAppear () {

        if (dialogueListAmountCounter < dialogueOutput.feedDialogueList.Count) {

            textBubble.SetActive (true);
            dialogueCharacter.SetActive (true);
        } else {
            dialogueDone = true;
            dialogueInput.dialogueEnded();
        }

    }

    IEnumerator OnTypeUpdate () {
        yield return new WaitForSeconds (time);
        if (dialogueInput.canType) {
            // Play Animation Talking state
            dialogueCharacterManager.OnTalking ();
            //if the dialogue bubble equals the feeded text amount or not
            if (text.text.Length < textBubble1.Length) {

                if (textBubble1[counter] == '<') {
                    float h = 1;
                    int tempCounter = counter;
                    while (textBubble1[tempCounter] != '>') {
                        h++;
                        tempCounter++;
                    }

                    for (var i = 0; i < h; i++) {
                        text.text += textBubble1[counter];
                        counter++;
                    }
                } else {

                    text.text += textBubble1[counter];
                    if (counter <= countAmount) {
                        counter++;

                    }
                }
            } else if (text.text.Length == textBubble1.Length) {
                dialogueListAmountCounter++;
                //Animation Quiet state
                dialogueCharacterManager.OnQuiet ();

                OnDisappear ();
                dialogueInput.CancleType ();
            }
            lockTime = 0;
        }
    }
}