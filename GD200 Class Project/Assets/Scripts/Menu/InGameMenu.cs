using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using UnityEngine.InputSystem;
using System;

public class InGameMenu : MonoBehaviour
{
    [SerializeField] Canvas menuCanvas;
    CharacterControls inputs;
    void Start()
    {   
        inputs = new CharacterControls();
        inputs.Enable();
        inputs.UI.Pause.performed += OnPause;
        menuCanvas.enabled = false;
    }

    private void OnPause(InputAction.CallbackContext obj)
    {
        OnPauseGame();
    }

    public void OnPauseGame(){
        menuCanvas.enabled = true;
        Time.timeScale = 0;
    }
    public void OnResumeGame(){
        menuCanvas.enabled = false;
        Time.timeScale = 1;
    }
    public void OnMenu(){
        if (menuCanvas.enabled == true)
        {
            
     SceneManager.LoadScene(0);   
        }
    }
}
