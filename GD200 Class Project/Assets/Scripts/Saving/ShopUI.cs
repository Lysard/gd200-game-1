using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class ShopUI : MonoBehaviour {
    [SerializeField] Canvas shopTextCanvas;
    [SerializeField] CharacterScript characterScript;
    [SerializeField] Canvas inShopUICanvas;
    [SerializeField] GameObject buyMenu;
    [SerializeField] CharacterBalanceScript characterBalanceScript;
    [SerializeField] CharacterHealthScript characterHealthScript;
    [SerializeField] GameObject teleportMenu;

    [SerializeField] float healingAmount;
    [SerializeField] float teleportAmount;
    [SerializeField] Transform swordPoint;
    [SerializeField] Transform bossPoint;
    [SerializeField] GameObject shopButtonCover;
    [SerializeField] GameObject teleportBossButton;
    
    [SerializeField] GameObject buyButton;
    [SerializeField] GameObject menuButton;
    [SerializeField] GameObject healBuyButton;

    private void Start () {
        shopTextCanvas.enabled = false;
        inShopUICanvas.enabled = false;
        buyMenu.SetActive (false);
        teleportMenu.SetActive(false);
        teleportBossButton.SetActive(false);
        

    }
    
    private void Update() {
        if (characterScript.upgradeList.Contains("Teleport"))
            { 
                
                teleportBossButton.SetActive(true);
            }

        if (inShopUICanvas.enabled == true)
        {
             UnityEngine.EventSystems.EventSystem.current.firstSelectedGameObject = buyButton;

        }else{
            UnityEngine.EventSystems.EventSystem.current.firstSelectedGameObject = menuButton;
        }
    }
    public void OnInteract () {
        inShopUICanvas.enabled = true;
     
        
        shopButtonCover.SetActive(false);
    }
    public void OnExit () {
        inShopUICanvas.enabled = false;
    }

    private void OnTriggerEnter2D (Collider2D other) {
        if (other.tag == "Player") {
            
            shopTextCanvas.enabled = true;
            characterScript.PlayerInButtonZone (true);

        }
    }
    public void OnTeleport(){
        if (characterBalanceScript.currencyBalance >= teleportAmount)
        {
            
        teleportMenu.SetActive(true);
        characterBalanceScript.OnBuyItem(teleportAmount);
        }
    }
    public void OnSwordTeleport(){
        characterScript.OnTeleport(swordPoint.position);
        teleportMenu.SetActive(false);
        buyMenu.SetActive (false);
        inShopUICanvas.enabled = false;
        characterScript.StopMovement(false);
        shopButtonCover.SetActive(false);
        
    }
    public void OnBossTeleport(){
        characterScript.OnTeleport(bossPoint.position);
        teleportMenu.SetActive(false);
        buyMenu.SetActive (false);
        inShopUICanvas.enabled = false;
        characterScript.StopMovement(false);
        shopButtonCover.SetActive(false);
    }

    public void OnBuyButton () {
        buyMenu.SetActive (true);
        shopButtonCover.SetActive(true);

    }
    public void OnBackButton () {
        buyMenu.SetActive (false);
        teleportMenu.SetActive(false);
        shopButtonCover.SetActive(false);
    }
    public void OnShopInteraction () {
        if (shopTextCanvas.enabled == true) {

            characterScript.StopMovement(true);
            inShopUICanvas.enabled = true;
        }
    }
    public void OnHealingBuy () {
        if (characterBalanceScript.currencyBalance >= healingAmount) {
            if (characterHealthScript.healingAmount == 0) {
                characterBalanceScript.OnBuyItem (healingAmount);
                characterHealthScript.OnHealBuy ();

            }
        }
    }
    public void OnTalk () {

    }

    public void OnExitShop () {
        Time.timeScale = 1;
        characterScript.StopMovement(false);
        inShopUICanvas.enabled = false;
    }
    private void OnTriggerExit2D (Collider2D other) {
        if (other.tag == "Player") {
            shopTextCanvas.enabled = false;
            characterScript.PlayerInButtonZone (false);
        }
    }
}