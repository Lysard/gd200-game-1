using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.InputSystem;
using UnityEngine.UI;

public class ManualSaveScript : MonoBehaviour {
    [SerializeField] Canvas saveCanvas;
    [SerializeField] CharacterScript characterScript;
    private CharacterControls inputs;
    private float savedInventoryCount;
    private Vector2 saveRespawnPoint;
    private float saveCount;
    [SerializeField] bool saved = false;
    [SerializeField] TextMeshProUGUI saveText;
    [SerializeField] Color textColor;
    [SerializeField] Color defaultColor;
    [SerializeField] CharacterBalanceScript characterBalanceScript;
    [SerializeField] CharacterHealthScript characterHealthScript;

    private void Start () {
        saveCanvas.enabled = false;
        inputs = new CharacterControls ();
        inputs.Enable ();

    }

    public void OnSave () {
        if (saveCanvas.enabled == true) {
            saveText.color = textColor;
            PlayerPrefs.SetFloat ("XPosition", saveRespawnPoint.x);
            PlayerPrefs.SetFloat ("YPosition", saveRespawnPoint.y);
            for (var i = 0; i < characterScript.upgradeList.Count; i++) {
                PlayerPrefs.SetString ($"inventory item{i}", characterScript.upgradeList[i]);

                savedInventoryCount = i;
            }
            PlayerPrefs.SetFloat ("savedInventoryCount", savedInventoryCount);
            saveCount++;
            PlayerPrefs.SetFloat ("saveCount", saveCount);
            PlayerPrefs.Save ();
            saved = true;
            PlayerPrefs.SetFloat("playerBalance", characterBalanceScript.currencyBalance);
            PlayerPrefs.SetFloat("playerHealingAmount", characterHealthScript.healingAmount);
            StartCoroutine(OnTextColorWhite());
        }
    }
    IEnumerator OnTextColorWhite () {
        yield return new WaitForSeconds (0.1f);
        saveText.color = defaultColor;
    }

    private void OnTriggerEnter2D (Collider2D other) {
        if (other.tag == "Player") {
            saveCanvas.enabled = true;
            characterScript.PlayerInButtonZone (true);
            saveRespawnPoint = other.transform.position;

        }
    }

    private void OnTriggerExit2D (Collider2D other) {
        if (other.tag == "Player") {
            saveCanvas.enabled = false;
            characterScript.PlayerInButtonZone (false);
        }
    }
}