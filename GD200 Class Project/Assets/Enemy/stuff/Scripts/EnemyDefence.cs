using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyDefence : MonoBehaviour
{
    [SerializeField] GameObject defenceCollider;
    [SerializeField] float defenceTime;
    [SerializeField] float defenceCancleTime;
    [SerializeField] Jump enemyMovementScript;
    [SerializeField] JumperAnimation animationScript;
    private bool isDefending = false;

    private void Start() {
        
    }
    public void OnDefence()
    {
        animationScript.OnDefenceTrigger();
        StartCoroutine(OnDefenceTimer());
    }
   public void cancleDefence()
   {
       StartCoroutine(OnDefenceCancle());
   }
   private void FixedUpdate() {
       animationScript.OnDefendBool(isDefending);
   }

    IEnumerator OnDefenceTimer()
    {
        yield return new WaitForSeconds(defenceTime);
       if (defenceCollider.activeSelf == false)
       {
           isDefending = true;
           
        defenceCollider.SetActive(true);   
       } 
       
    }
    IEnumerator OnDefenceCancle()
    {
       yield return new WaitForSeconds(defenceTime);
       if (enemyMovementScript.seesPlayer == false)
       {
           isDefending = false;
        defenceCollider.SetActive(false); 
        animationScript.OnIdleTrigger();
        enemyMovementScript.SetIdleState();
           
       }
    }
}
