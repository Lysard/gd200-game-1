using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Jump : MonoBehaviour {
    public enum EnemyStates {
        Idle,
        Jumping,
        Defending
    }

    [SerializeField] JumperAnimation animationScript;
    [SerializeField] Vector2 force;
    [SerializeField] float idleTime;
    [SerializeField] EnemyStates enemyStates;
    [SerializeField] bool isGrounded;
    [SerializeField] float groundCheckRadius;
    [SerializeField] LayerMask groundLayer;
    private float idleTimer;
    private bool jumped = false;
    [SerializeField] bool facingRight;
    public bool seesPlayer;
    [SerializeField] float playerSightRadius;
    [SerializeField] LayerMask playerLayer;
    [SerializeField] Rigidbody2D rb2d;
    private bool isDefending = false;

    void Start () {

        enemyStates = EnemyStates.Idle;

    }

    void FixedUpdate () {
        animationScript.OnGroundedBool (isGrounded);
        isGrounded = Physics2D.OverlapCircle (rb2d.position, groundCheckRadius, groundLayer);
        seesPlayer = Physics2D.OverlapCircle (rb2d.position, playerSightRadius, playerLayer);
        switch (enemyStates) {
            case EnemyStates.Idle:
                OnIdle ();
                animationScript.OnIdleTrigger ();
               
                if (!isGrounded) {
                    enemyStates = EnemyStates.Jumping;

                }
                break;
            case EnemyStates.Jumping:

                

                OnJumping ();

                break;
            case EnemyStates.Defending:
               
                
                OnDefend ();
                break;
        }

    }
    private void Update () {
        rb2d.constraints = RigidbodyConstraints2D.FreezeRotation;

    }
    private void OnIdle () {

        if (isGrounded) {

            switch (seesPlayer) {
                case true:
                    enemyStates = EnemyStates.Defending;
                    break;
                case false:
                    idleTimer--;
                    if (idleTimer < 0) {
                        checkFacing ();
                        jumped = false;
                        rb2d.constraints = RigidbodyConstraints2D.None;
                        animationScript.OnJumpTrigger ();
                        rb2d.AddForce (force, ForceMode2D.Impulse);
                        flipForce ();

                        idleTimer = idleTime;
                    }
                    break;
            }

        }

    }
    private void OnDefend () {
        if (seesPlayer) {

            rb2d.constraints = RigidbodyConstraints2D.FreezePositionX;
            transform.GetComponent<EnemyDefence> ().OnDefence ();

        } else {

            if (!seesPlayer && isGrounded && enemyStates == EnemyStates.Defending) {
            rb2d.constraints = RigidbodyConstraints2D.None;
            transform.GetComponent<EnemyDefence> ().cancleDefence ();

        }
        }
    }
    private void checkFacing () {
        if (force.x > 0) {
            facingRight = true;
            Vector2 theScale = transform.localScale;
            theScale.x = 1;
            transform.localScale = theScale;
        } else if (force.x < 0) {
            facingRight = false;
            Vector2 theScale = transform.localScale;
            theScale.x *= -1;
            transform.localScale = theScale;
        }
    }
    private void flipForce () {
        force.x = -force.x;
    }

    private void OnJumping () {
        if (isGrounded) {
            rb2d.velocity = Vector2.zero;
            rb2d.constraints = RigidbodyConstraints2D.FreezePositionX;

            enemyStates = EnemyStates.Idle;

        }
    }
    IEnumerator DefenseWaitIdle () {
        yield return new WaitForSeconds (0.5f);
        
    }
    public void SetIdleState () {
        enemyStates = EnemyStates.Idle;
    }

}