using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class EnemyHealth : MonoBehaviour
{
    public float enemyHealth = 100;
    [SerializeField] float maxEnemyHealth;
    [SerializeField] Image healthBar;
    [SerializeField] GameObject enemy;
    [SerializeField] Transform enemyGraveyard;
    [SerializeField] Transform activeEnemyPool;
    [SerializeField] Transform currencyActivePool;
    [SerializeField] Transform currencyPool;
    [SerializeField] Rigidbody2D currency;

    private void Start() {
        maxEnemyHealth = enemyHealth;
    }
    private void Update() {
        healthBar.fillAmount = enemyHealth/maxEnemyHealth;
        if (enemyHealth <= 0)
        {
            OnDeath();
        }
        
    }

    public void OnAttack(float damage)
    {
        enemyHealth -= damage;
    }
    public bool AfterAttack()
    {
        return false;
    }
    private void OnDeath()
    {
        Debug.Log("enemy dies");
        currency = currencyPool.GetChild(0).GetComponent<Rigidbody2D>();
        currency.transform.parent = currencyActivePool;
        currency.position = enemy.transform.position;
        enemy.transform.parent = enemyGraveyard; 
    }
    private void OnTriggerEnter2D(Collider2D other) {
        if (other.tag == "Spike")
        {
            OnDeath();
        }
    }


}
