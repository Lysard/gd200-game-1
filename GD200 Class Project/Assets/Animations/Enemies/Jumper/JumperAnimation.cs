using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class JumperAnimation : MonoBehaviour
{
    [SerializeField] Animator animator;
    void Start()
    {
        animator = gameObject.GetComponent<Animator>();
    }

    public void OnIdleTrigger(){
        animator.SetTrigger("IdleTrigger");
    }
    public void OnDefendBool(bool isDefending){
        animator.SetBool("isDefending",isDefending);
    }
    public void OnDefenceTrigger(){
        animator.SetTrigger("DefendTrigger");
    }
    public void OnJumpTrigger(){
        animator.SetTrigger("JumpTrigger"); 
    }
    public void OnGroundedBool(bool isGrounded){
        animator.SetBool("isGrounded", isGrounded);
    }
}
