using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ChaseEnemyAnimation : MonoBehaviour
{
    [SerializeField] Animator animator;

    private void Start() {
        animator = gameObject.GetComponent<Animator>();
    }
   public void OnIdleTrigger(){
       animator.SetTrigger("IdleTrigger");
   }

   public void OnSeePlayerBool(bool seesPlayer){
       animator.SetBool("seesPlayer",seesPlayer);
   }

   public void OnRunningTrigger(){
       animator.SetTrigger("RunningTrigger");
   }
   public void OnWalkingTrigger(){
       animator.SetTrigger("WalkingTrigger");

   }
}
