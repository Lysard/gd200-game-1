using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CrawlerAnimation : MonoBehaviour
{
    Animator animator;


    private void Awake() {
        animator = GetComponent<Animator>();
    }

    public void OnIdleTrigger(){
        animator.SetTrigger("IdleTrigger");
    }

    public void OnFlyingTrigger(){
        animator.SetTrigger("FlyingTrigger");
    }
}
